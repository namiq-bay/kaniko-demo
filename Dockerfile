FROM alpine:3.14.4
RUN apk add --no-cache openjdk11
COPY build/libs/kaniko-demo-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/kaniko-demo-0.0.1-SNAPSHOT.jar"]
