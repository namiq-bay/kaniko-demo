This is simple CI/CD project with kaniko image builder. 

kaniko doesn't depend on a Docker daemon and executes each command within a Dockerfile completely in userspace.


![image](/uploads/b767b584dc0f4df5f7ffb0c7bb8336ef/image.png)


Related article: https://medium.com/p/84f816d86a6
