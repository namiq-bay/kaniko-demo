package com.example.kanikodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KanikoDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(KanikoDemoApplication.class, args);
    }

}
